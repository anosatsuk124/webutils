import { PrimaryContainer } from './Components/Container';
import WebcamCapture from './Pages/Webcam';

const App = () => {
    return (
        <div className="md:container md:mx-auto py-5">
            <PrimaryContainer>
                <WebcamCapture />
            </PrimaryContainer>
        </div>
    );
};

export default App;
