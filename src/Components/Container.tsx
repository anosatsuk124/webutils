interface PrimaryContainerProps {
    children?: React.ReactNode;
    className?: string;
}

export const PrimaryContainer = (props: PrimaryContainerProps) => {
    const className = `md:mx-auto py-4 ${
        props.className ? props.className : ''
    }`;

    return <div className={className}>{props.children}</div>;
};
