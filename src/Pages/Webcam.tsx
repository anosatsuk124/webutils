import { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react';
import Webcam from 'react-webcam';
import { PrimaryContainer } from '../Components/Container';
import { useAtom } from 'jotai';
import { WebcamConstraintsAtom } from '../atoms';

const ConfigureConstraints = () => {
    const [constraints, setConstraints] = useAtom(WebcamConstraintsAtom);

    const onChangeHeight = useCallback(
        (event: ChangeEvent<HTMLInputElement>) => {
            setConstraints({
                ...constraints,
                width: constraints?.width,
                height: parseInt(event.target.value),
            });
        },
        [constraints, setConstraints]
    );

    const onChangeWidth = useCallback(
        (event: ChangeEvent<HTMLInputElement>) => {
            setConstraints({
                ...constraints,
                height: constraints?.height,
                width: parseInt(event.target.value),
            });
        },
        [constraints, setConstraints]
    );

    return (
        <PrimaryContainer>
            <div>
                <input
                    className="grid input input-bordered w-full max-w-xs"
                    type="number"
                    placeholder="Width"
                    value={constraints?.width}
                    onChange={onChangeWidth}
                />
                <input
                    className="grid input input-bordered w-full max-w-xs"
                    type="number"
                    placeholder="Height"
                    value={constraints?.height}
                    onChange={onChangeHeight}
                />
            </div>
        </PrimaryContainer>
    );
};

const WebcamCapture = () => {
    const [constraints, setConstraints] = useAtom(WebcamConstraintsAtom);

    const [devices, setDevices] = useState<MediaDeviceInfo[]>([]);
    const [audioDevices, setAudioDevices] = useState<MediaDeviceInfo[]>([]);
    const [audioDeviceId, setAudioDeviceId] = useState<string>('');

    const webcamRef = useRef<Webcam>(null);

    useEffect(() => {
        const deviceId = localStorage.getItem('deviceId');
        if (deviceId) {
            setConstraints({
                ...constraints,
                deviceId,
            });
        }

        const id = localStorage.getItem('audioDeviceId');
        if (id) {
            setAudioDeviceId(id);
        }
    }, [setConstraints, constraints, setAudioDeviceId]);

    const handleDevices = useCallback(
        (mediaDevices: MediaDeviceInfo[]) => {
            setDevices(
                mediaDevices.filter(({ kind }) => kind === 'videoinput')
            );

            setAudioDevices(
                mediaDevices.filter(({ kind }) => kind === 'audioinput')
            );
        },
        [setDevices, setAudioDevices]
    );

    const onSelectVideo = useCallback(
        (event: ChangeEvent<HTMLSelectElement>) => {
            setConstraints({
                ...constraints,
                deviceId: event.target.value,
            });
            localStorage.setItem('deviceId', event.target.value);
        },
        [setConstraints, constraints]
    );

    const onSelectAudio = useCallback(
        (event: ChangeEvent<HTMLSelectElement>) => {
            setAudioDeviceId(event.target.value);
            localStorage.setItem('audioDeviceId', audioDeviceId);
        },
        [setAudioDeviceId, audioDeviceId]
    );

    const refreshDevices = useCallback(() => {
        navigator.mediaDevices.enumerateDevices().then(handleDevices);
    }, [handleDevices]);

    const intoFullscreen = useCallback(() => {
        const video = webcamRef.current?.video;
        if (video) {
            video.requestFullscreen();
        }
    }, []);

    return (
        <PrimaryContainer>
            <Webcam
                controls={true}
                audio={true}
                height={constraints?.height}
                width={constraints?.width}
                videoConstraints={constraints}
                audioConstraints={{
                    deviceId: audioDeviceId,
                    channelCount: 2,
                    echoCancellation: false,
                    noiseSuppression: false,
                    autoGainControl: false,
                }}
                ref={webcamRef}
            />
            <select
                className="select select-bordered w-full max-w-xs"
                onChange={onSelectVideo}
            >
                {devices.map((device, key) => (
                    <option key={key} value={device.deviceId}>
                        {device.label}
                    </option>
                ))}
            </select>
            <select
                className="select select-bordered w-full max-w-xs"
                onChange={onSelectAudio}
            >
                {audioDevices.map((device, key) => (
                    <option key={key} value={device.deviceId}>
                        {device.label}
                    </option>
                ))}
            </select>
            <button className="btn btn-primary" onClick={refreshDevices}>
                Refresh Devices
            </button>
            <button className="btn" onClick={intoFullscreen}>
                Fullscreen
            </button>
            <ConfigureConstraints />
        </PrimaryContainer>
    );
};

export default WebcamCapture;
