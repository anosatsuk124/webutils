import { atom } from 'jotai';

interface WebcamConstraints {
    width: number;
    height: number;
    frameRate: number;
    deviceId?: string;
}

export const WebcamConstraintsAtom = atom<WebcamConstraints>({
    width: 1920,
    height: 1080,
    frameRate: 60,
});
